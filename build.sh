#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$DIR/common.sh"

export GOPATH="$GOPATH"
pushd "$DIR"
go get -v "./..."
popd

go build -o "$DIR/subscriber" "$DIR/subscriber.go"
