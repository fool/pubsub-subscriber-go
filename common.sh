#!/bin/bash

function log() {
	local level="$1"
	local message="$2"
	echo "$level [$(date +"%Y-%m-%dT%H:%M:%S%:z")] $message"
}

GOOGLE_APPLICATION_CREDENTIALS="$PWD/../../twow-7c3f49b3ee94.json"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
GOPATH="$DIR"
