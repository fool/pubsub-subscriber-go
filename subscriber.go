package main

import (
    "encoding/json"
	"sync"
    "os"
	"os/exec"
    "flag"
    "regexp"
    "fmt"
    "strconv"
    "strings"
    "errors"
    "github.com/op/go-logging"
    "golang.org/x/net/context"
    "cloud.google.com/go/pubsub"
    "google.golang.org/api/iterator"
    "google.golang.org/api/option"
)

var log = logging.MustGetLogger("publisher")
var logFormat = logging.MustStringFormatter(
	`%{level} [%{time:15:04:05.000}] %{message}`,
)

type CliArgs struct {
    projectId *string
    pubSubTopicId *string
    subscriptionName *string
    program *string
    credentials *string
    createSubscriptionIfNotExists bool
}

/**
 * Parse arguments and initialize logging
 * @return CliArgs if things are well formed, error otherwise
 */
func parseArgs() (CliArgs, error) {
    var args CliArgs
    backend := logging.AddModuleLevel(logging.NewBackendFormatter(logging.NewLogBackend(os.Stdout, "", 0), logFormat))
    DefaultLogLevel := "info"

    projectId := flag.String("project-id", "", "Project ID")
    pubSubTopicId := flag.String("topic-id", "", "PubSub Topic ID")
    subscriptionName := flag.String("subscription", "", "Subscription name like projects/<project>/subscriptions/<subscription name>")
    program := flag.String("program", "", "Program to execute when a message is received. The message will be the only CLI argument")
    credentials := flag.String("credentials", "", "Optional google JSON credentials file. If not provided you need to use 'gcloud auth application-default login'")
    logLevelString := flag.String("log-level", DefaultLogLevel, "Optional log level. Choices: 'debug', 'info', 'notice', 'warning', 'error', 'critical'")
    createIfNotExistsString := flag.Bool("create-subscription-if-missing", true, "If the subscription does not exist, create it. Defaults to true")
    flag.Parse()


    parseError := false

    logLevel, err := logging.LogLevel(*logLevelString)
    if err != nil {
        logging.SetBackend(backend)
        backend.SetLevel(logging.INFO, "")
        log.Error("Unable to parse log level:", *logLevelString, "Must be one of: 'debug', 'info', 'notice', 'warning', 'error', 'critical'")
        os.Exit(3)
    }
    backend.SetLevel(logLevel, "")
    logging.SetBackend(backend)


    if *projectId == "" {
        log.Error("--project-id <proj id> is required")
        parseError = true
    }
    if *pubSubTopicId == "" {
        log.Error("--topic-id <topic> is required")
        parseError = true
    }
    if *subscriptionName == "" {
        log.Error("--subscription <subscription-name> is required")
        parseError = true
    }

    if parseError {
        return args, errors.New("parse error")
    } else {
        args = CliArgs{projectId, pubSubTopicId, subscriptionName, program, credentials, *createIfNotExistsString}
        return args, nil
    }
}

func main() {
    cliArgs, err := parseArgs()
    if err != nil {
        os.Exit(1)
    }

    ctx := context.Background()
    log.Debug("Creating new client")
    client := createClient(cliArgs.credentials, ctx, cliArgs.projectId)
    if err != nil {
	    log.Error("Error creating PubSub client %s", err)
        os.Exit(2)
    }

    log.Info("Fetching topic", *cliArgs.pubSubTopicId)
    topic := client.Topic(*cliArgs.pubSubTopicId)
    var subscription = getSubscription(client, topic, cliArgs.subscriptionName)
    if subscription == nil {
        if cliArgs.createSubscriptionIfNotExists {
            subscription = createSubscription(client, topic, getShortSubscriptionName(cliArgs.subscriptionName))
        } else {
            log.Error(fmt.Sprintf("Unable to locate subscription '%s'. You can create the subscription with --create-subscription-if-missing", *cliArgs.subscriptionName))
            os.Exit(3)
        }
    }

    log.Debug("Calling subscribe go routine")
    var wg sync.WaitGroup
    wg.Add(1)
    go subscribe(subscription, &wg, cliArgs.program)
    log.Debug("Waiting on waitgroup in main()")
    wg.Wait()
    log.Info("Exiting")
}

/**
 * Peel off the last slash of the subscription or exit the program
 *
 * @param fullSuscriptionName Ex: /projects/PROJECT_NAME/subscriptions/SUBSCRIPTION_NAME
 * @return Just the last part, using example above, SUBSCRIPTION_NAME
 */
func getShortSubscriptionName(fullSubscriptionName *string) string {
    var subscriptionRegex = `/?.+/.+/.+/(.+)$`
    r := regexp.MustCompile(subscriptionRegex)
    matches := r.FindStringSubmatch(*fullSubscriptionName)
    if len(matches) < 2 {
        // TODO - Accept this as a CLI arg
        log.Error(fmt.Sprintf("Subscription was not found. Tried making the subscription, but unable to extract subscription name automatically from '%s' using regex %s", fullSubscriptionName, subscriptionRegex))
        os.Exit(12)
    }
    return matches[1]
}


/**
 * Make a pubsub client
 *
 * @param credentials Optional file name containing JSON google credentials. Can be empty to use
 *                    background authentication
 * @param ctx         ???
 * @param projectId   String project id for google cloud
 * @return            the client
 */
func createClient(credentials *string, ctx context.Context, projectId *string) *pubsub.Client {
    if *credentials != "" {
        log.Debug("Using credentials from file", *credentials)
        client, err := pubsub.NewClient(ctx, *projectId, option.WithCredentialsFile(*credentials))
        if err != nil {
            log.Error("Error creating a pub sub client:", err)
            os.Exit(6)
        }
        return client
    } else {
        client, err := pubsub.NewClient(ctx, *projectId)
        if err != nil {
            log.Error("Error creating a pub sub client:", err)
            if strings.Contains(err.Error(), "could not find default credentials") {
                log.Error("You need to login using 'gcloud auth' command line, use google role permissions, or pass in a JSON credentials file using --credentials <file>")
            }
            os.Exit(7)
        }
        return client
    }
}

/**
 * Retrieves a subscription object
 * @param client PubSub client
 * @param topic  The topic name
 * @param fullSubscriptionName The full subscription path, ex: "/projects/PROJECT_NAME/subscriptions/SUBSCRIPTION_NAME"
 * @return subscription or Nil if not found
 */
func getSubscription(client *pubsub.Client, topic *pubsub.Topic, fullSubscriptionName *string) *pubsub.Subscription {
    ctx := context.Background()
	log.Debug("Listing all subscriptions")
	it := client.Subscriptions(ctx)
	log.Debug("Got subscriptions, looking for the main one")
	for {
		s, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Error("error going through subscriptions iterator ", err)
			os.Exit(11)
		}
		if s.String() == *fullSubscriptionName {
		  return s
		}
    }
    return nil
}

/**
 * Creates a new subscription
 * @param client PubSub client
 * @param topic PubSub topic
 * @param subscriptionName The name of the subscription. This should not be the full path, just the name
 */
func createSubscription(client *pubsub.Client, topic *pubsub.Topic, subscriptionName string) *pubsub.Subscription {
    log.Info("Subscription not found, creating new subscription with name", subscriptionName)
    ctx := context.Background()
    var subscription, subErr = client.CreateSubscription(ctx, subscriptionName, pubsub.SubscriptionConfig{Topic: topic})
    if subErr != nil {
      log.Error("Unable to make a subscription, saw error %s", subErr)
      os.Exit(12)
    }
    log.Info("Subscription create successfully")
    return subscription
}

/**
 * Called whenever a message is received through PubSub
 * @param ctx ???
 * @param msg PubSub message
 * @return nil
 */
func receiveMessage(ctx context.Context, msg *pubsub.Message, program *string) {
    log.Debug(fmt.Sprintf("%s received, converting from byte array (len=%d, %d attributes) to string", msg.ID, len(msg.Data), len(msg.Attributes)))
    var message = string(msg.Data)
    var publishTime = strconv.FormatInt(msg.PublishTime.UnixNano(), 10)
    var hasAttributes = len(msg.Attributes) > 0
    var numArgsToProgram = 6
    if hasAttributes {
        numArgsToProgram = 8
    }
    var args = make([]string, numArgsToProgram, numArgsToProgram)
    args[0] = "--id"
    args[1] = msg.ID
    args[2] = "--message"
    args[3] = message
    args[4] = "--publish-time"
    args[5] = publishTime

    if hasAttributes {
        jsonString, err := json.Marshal(msg.Attributes)
        if err != nil {
            log.Error(fmt.Sprintf("%s Unable to convert map[string] to json string, sending NACK to pubsub. Error was: %s", msg.ID, err))
            msg.Nack()
            return
        } else {
            args[6] = "--attributes"
            args[7] = string(jsonString)
        }
    }

    log.Info(fmt.Sprintf("%s Executing: %s %s", msg.ID, *program, strings.Join(args, " ")))
    cmd := exec.Command(*program, args...)
    err := cmd.Run()
    if err != nil {
      log.Error(fmt.Sprintf("%s error while executing program, sending Nack: %s", msg.ID, err))
      msg.Nack()
    } else {
      log.Info(fmt.Sprintf("%s Command was successful, sending Ack to pubsub", msg.ID))
      msg.Ack()
    }
}

/**
 * Subscribe to a topic
 * @param subscription Subscription to listen to
 * @param wg           ???
 * @param program      The program to run when a message is received
 * @return nil
 */
func subscribe(subscription *pubsub.Subscription, wg *sync.WaitGroup, program *string) {
	ctx := context.Background()
	log.Info("Starting to receive messages")
	err := subscription.Receive(ctx, func(ctx context.Context, msg *pubsub.Message) {
	    receiveMessage(ctx, msg, program)
    })

    log.Info("Subscription has been stopped. Will not receive any more messages")
	if err != nil {
		log.Error(err)
		os.Exit(20)
	}
	log.Debug("In subscribe, telling wg i'm done")
	defer wg.Done()
}
