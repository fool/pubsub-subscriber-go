# Subscriber

Go program to listen on a PubSub topic and calls a program when there's a message. If your program exits with 0 then the message is ACK'd, if your program exits with non-zero then the message is NACK'd

# Usage
```bash
subscriber \
    --project-id my-project-id \
    --topic-id my-topic-id \
    --subscription "projects/my-project-id/subscriptions/my-subscription-id" \
    --program /absolute/path/to/my/script.sh
```
This will call `script.sh` with the following arguments:
```bash
/absolute/path/to/my/script.sh \
    --id <PubSub Message ID> \
    --message <PubSub Message> \
    --publish-time <When the message was created in epoch nanoseconds> \
    --attributes <JSON Object as String>  # If the message had any attributes a JSON object of them
```
- Your program should exit with 0 if the message can be `Ack` and deleted
- Your program should exit with non-zero if the message should be `Nack` and retried
- If your program exits with 0 there is still a chance the message could be retried if the `Ack` fails

##### How long does the program have before the message is `Nack`'d automatically?
Check your subscription's settings. This can make a subscription for you but you will only have
the default subscription settings applied.
 
### What other options are there?

Try `subscriber --help` for more options
# Building
```bash
make compile
```

# Developing
Use `./run.sh` to build and test the program like
```bash
./run.sh --project-id pj --topic-id topic --subscription "projects/pj/subscriptions/subscription" --program /program.sh
```
